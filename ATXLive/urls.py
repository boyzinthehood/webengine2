"""ATXLive URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include, patterns
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import recommendation.views
import chatroom.views

urlpatterns = [
 url(r'^all/', recommendation.views.EventListView.as_view(), name = 'event-list'),
 url(r'^add/', recommendation.views.add_event,name='event-new'),
 url(r'^', include('recommendation.urls')),
 url(r'^recommendation/', include('recommendation.urls')),
 url(r'^recommendation/(?P<event>[a-zA-Z0-9_/-]+)/', recommendation.views.event_base, name='eventbase'),
 url(r'^chatroom/', include('chatroom.urls')),
 url(r'^admin/', admin.site.urls),
 url(r'^about/', recommendation.views.about, name = 'about'),
 url(r'^login_view/', recommendation.views.login_view, name='login_view'),
 url(r'^register/', recommendation.views.register, name='register'),
 url(r'^index/(?P<event>[a-zA-Z0-9_/-]+)/$', recommendation.views.event_base, name='eventbase'),
 #url(r'^index/', recommendation.views.index, name='index'),
 url(r'^eventslist/', recommendation.views.eventslist, name='eventslist'),
 url(r'^logout/', recommendation.views.logout_page, name='logout'),
 url(r'^chatroom/home/', chatroom.views.Home, name='chatroom'),
 url(r'^(?P<event>[a-zA-Z0-9_/-]+)/', recommendation.views.event_base, name='eventbase'),

]

urlpatterns += staticfiles_urlpatterns()
