# README #

This project aims to provide Austinites with the ability to quickly make plans and connect with other Austinites on any given day.

Done:
- Project Setup
- Event model
- Bare bones landing page
- Bare bones add new events and view all events


To do:
- Delete events
- User model
- Recommendation Engine 
- Integrating static assets into Django project
- Chatroom
- Deployment
- Login page
- File Uploader (for pictures and stuff I guess?)
- Calendar page

Metaproject work:
HW2 write up (including how we used design patterns or what not)
Putting together the presentation


