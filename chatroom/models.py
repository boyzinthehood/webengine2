from django.db import models
from django.contrib.auth.models import User
from recommendation.models import Event

class Chat(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)
    message = models.CharField(max_length=200)
    event = models.CharField(max_length=255, blank=True)
    def __unicode__(self):
        return self.message
