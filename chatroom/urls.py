from django.conf.urls import url
from django.views.generic import RedirectView

from . import views

urlpatterns = [
   # url(r'^$', RedirectView.as_view(url='home/')),
    url(r'^login/$', views.chatLogin, name='chatlogin'),
    url(r'^logout/$', views.Logout, name='logout'),
    url(r'^home/$', views.Home, name='home'),
    url(r'^post/$', views.Post, name='post'),
    url(r'^messages/(?P<event_name>[a-zA-Z0-9_\-]*)$', views.Messages, name='messages'),
    url(r'^events/(?P<event_name>[a-zA-Z0-9_\-]*)/$', views.Events, name='events'),
]
