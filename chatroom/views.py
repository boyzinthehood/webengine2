from django.shortcuts import render
from django.contrib.auth import authenticate, logout, login
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from ATXLive import settings

from .models import Chat
from recommendation.models import Event


def chatLogin(request):
    next = request.GET.get('next', '/chatroom/home/')
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(next)
            else:
                return HttpResponse("Account is not active at the moment.")
        else:
            return HttpResponseRedirect(settings.LOGIN_URL)
    return render(request, "chatroom/login.html", {'next': next})

def Logout(request):
    logout(request)
    return HttpResponseRedirect('/chatroom/login/')

def Home(request):
    print(">>>>>>>>>>>> HOME <<<<<<<<<<<<<<<<<")
    c = Chat.objects.filter(event='')
    return render(request, "chatroom/home.html", {'home': 'active', 'chat': c})

def Post(request):
    if request.method == "POST":
        msg = request.POST.get('msgbox', None)
        event_title = request.POST.get('event_title', None)
        c = Chat(user=request.user, message=msg, event=event_title.replace(" ", "-").lower())
        if msg != '':
            c.save()
        return JsonResponse({ 'msg': msg, 'user': c.user.username })
    else:
        return HttpResponse('Request must be POST.')

def Messages(request, event_name):
    if event_name == 'null':
        c = Chat.objects.all()
    else:
        c = Chat.objects.filter(event=event_name.replace(" ", "-").lower())
    return render(request, 'chatroom/messages.html', {'chat': c})

def Events(request, event_name):
    print(">>>>>>>>>>>>>>>>> EVENTS <<<<<<<<<<<<<<<<<<<<<<")
    print(event_name)
    print(event_name.replace("-", " "))
    try:
        e = Event.objects.get(title__iexact=event_name.replace("-", " "))
        c = Chat.objects.filter(event=event_name.lower())
        return render(request, 'chatroom/eventChat.html', {'event': e, 'chat': c})
    except Event.DoesNotExist:
        return render(request, 'chatroom/eventNotFound.html')
