import os
import random
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ATXLive.settings')

import django
django.setup()
from recommendation.models import Event
from recommendation.models import create_event


for e in Event.objects.all():
    print (e.title)
    print (e.date)
    print (e.startTime)
    print (e.duration_in_minutes)
    print (e.email)
    print (e.approximatePrice)
    print (e.event_type)
