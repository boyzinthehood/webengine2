import os
import random
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ATXLive.settings')

import django
django.setup()
from recommendation import models
from recommendation.models import UserEvents
from django.contrib.auth.models import User
from datetime import date


def make_events():
    todate = date(2016, 9, 11)
    e1 = models.create_event('Surprise Party', todate, '7:00 PM', 60, User.objects.get(username='jcur'), 'user.email', '$', 'SOC', 'Chuck E. Cheese', 'Come to my 21st!!!')
    models.attend_event(UserEvents.objects.get(myUser=User.objects.get(username='jcur')), e1)

    e2 = models.create_event('Video Game Night', todate, '3:00 AM', 60, User.objects.get(username='jzhang'), 'user.email', '$$', 'SOC', 'Chez Moi', 'Nazi Zombies')
    models.attend_event(UserEvents.objects.get(myUser=User.objects.get(username='jzhang')), e2)

    e3 = models.create_event('Calligraphy Session', todate, '2:00 PM ', 60, User.objects.get(username='kwang'), 'user.email', '$$$', 'PRO', 'here', 'Come study under the master')
    models.attend_event(UserEvents.objects.get(myUser=User.objects.get(username='kwang')), e3)

    e4 = models.create_event('Tea Time', todate, '10:00 AM', 60, User.objects.get(username='csimp'), 'user.email', '$', 'SOC', 'UTeaHouse', 'Come enjoy quality tea')
    models.attend_event(UserEvents.objects.get(myUser=User.objects.get(username='csimp')), e4)

    e5 = models.create_event('Programming Session', todate, '6:00 AM', 60, User.objects.get(username='jthom'), 'user.email', '$$', 'PRO', 'Programming Wunderland', 'For rockstar unicorn ninja programmers')
    models.attend_event(UserEvents.objects.get(myUser=User.objects.get(username='jthom')), e5)

    e11 = models.create_event('Music Festival', todate, '12:00 AM', 60, User.objects.get(username='jcur'), 'user.email', '$', 'SOC', 'ACL', 'THE BIGGEST BADDEST MUSIC FESTIVAL')
    models.attend_event(UserEvents.objects.get(myUser=User.objects.get(username='jcur')), e11)

    e12 = models.create_event('Violin Practice Session', todate, '6:00', 60, User.objects.get(username='jzhang'), 'user.email', '$', 'PRO', 'DSO', 'Violinists are the smarter players')
    models.attend_event(UserEvents.objects.get(myUser=User.objects.get(username='jcur')), e12)

    e_list = [e1, e2, e3, e4, e5, e11, e12]
    return e_list

e = make_events()
for events in e:
    print (events.title)

