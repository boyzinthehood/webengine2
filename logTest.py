import os
import random
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ATXLive.settings')

import django
django.setup()
from django.contrib.auth.models import User
from recommendation.models import create_user

def populate():
    create_user("jcur", "461995", "curewitz@yahoo.com", "Justin", "Curewitz")
    create_user("jzhang", "321995", "fag@yahoo.com", "James", "Zhang")
    create_user("kwang", "111995", "kwang@yahoo.com", "Kristian", "Wang")
    create_user("csimp", "111995", "csimp@yahoo.com", "Clint", "Simpson")
    create_user("jthom", "111995", "jthom@yahoo.com", "Joey", "Thometz")


populate()
for u in User.objects.all():
    print(u.first_name)
