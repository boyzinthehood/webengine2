from django import forms
from django.contrib.auth.models import User
from recommendation.models import Event


class RegisterForm(forms.ModelForm):
    username = forms.CharField(max_length=255, help_text="Enter Username")
    password = forms.CharField(max_length=255, help_text="Enter Password", widget=forms.PasswordInput())
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    email = forms.CharField(required=False)

    class Meta:
        model = User
        fields = ('username', 'password',)


class LoginForm(forms.Form):
    username = forms.CharField(max_length=255)
    password = forms.CharField(max_length=255, widget=forms.PasswordInput())


class AddEventForm(forms.ModelForm):
    title = forms.CharField(max_length=255, help_text="Enter Title", required = False)
    date = forms.DateField(help_text="Date", required = False)
    startTime = forms.CharField(help_text="Start Time", required = False)
    duration_in_minutes = forms.IntegerField(help_text="Duration", required = False)
    host = User
    email = forms.EmailField(help_text="Email", required = False)
    page = forms.URLField(required=False)
    money = (('$', 'Cheap'), ('$$', 'Affordable'), ('$$$', 'Expensive'))
    approximatePrice = forms.ChoiceField(choices=money, required=True)
    location = forms.CharField(help_text="Location")
    types_of_event = (('SOC', 'Social'), ('ACA', 'Academic'), ('PRO', 'Professional'))
    event_type = forms.ChoiceField(choices=types_of_event, required=True)
    description = forms.CharField(max_length=1000, help_text='Describe it')

    class Meta:
        model = Event
        exclude = ('host', 'page', 'full_name')


class RSVPForm(forms.Form):
    e_title = forms.CharField(max_length=255, required=False, disabled=True)

