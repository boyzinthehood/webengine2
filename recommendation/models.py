from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User


class Event(models.Model):
    title = models.CharField(max_length=255, unique=True)
    date = models.DateField(False,False)
    startTime = models.CharField(max_length=10, default = '12:00am')
    duration_in_minutes = models.PositiveIntegerField()
    host = User
    email = models.EmailField()
    page = models.URLField()
    money = (('$', 'Cheap'), ('$$', 'affordable'), ('$$$', 'expensive'))
    approximatePrice = models.CharField(max_length=3, choices=money, default='$')
    types_of_event = (('SOC', 'Social'), ('ACA', 'Academic'), ('PRO', 'Professional'))
    event_type = models.CharField(max_length=3, choices=types_of_event, default='SOC')
    location = models.CharField(max_length=255, default = 'Austin, TX')
    description = models.CharField(max_length=1000, default='This is an event.')
    full_name = models.CharField(max_length=255, default='John Snow')

    #override
    def __str__(self):
        return self.title


class UserEvents(models.Model):
    myUser = models.OneToOneField(User)
    event_list = models.ManyToManyField(Event, related_name='myevents')

    def __str__(self):
        return self.myUser.username
        

def create_event(title, date, time, duration, host, email, price, event_type, location, description):
    e = Event()
    e.title = title
    e.date = date
    e.startTime = time
    e.duration_in_minutes = duration
    e.host = host
    e.email = email
    e.approximatePrice = price
    e.event_type = event_type
    e.location = location
    e.description = description
    e.page = slugify(e.title)
    e.full_name = host.get_full_name()
    e.save()
    re = singleton_engine()
    re.add_event(e)
    return e


def create_user(username, password, email, first_name, last_name):
    u = User()
    u.username = username
    u.set_password(password)
    u.first_name = first_name
    u.last_name = last_name
    u.email = email
    u.save()
    ue = set_user(u)
    return ue


def set_user(user):
    ue = UserEvents(myUser=user)
    ue.save()
    return ue


def attend_event(user, event):
    user.event_list.add(event)


class RecommendationEngine(models.Model):
    SOCCHP = models.ManyToManyField(Event, related_name="SOCCHP")
    SOCAFF = models.ManyToManyField(Event, related_name='SOCAFF')
    SOCEXP = models.ManyToManyField(Event, related_name='SOCEXP')
    ACACHP = models.ManyToManyField(Event, related_name='ACACHP')
    ACAAFF = models.ManyToManyField(Event, related_name='ACAAFF')
    ACAEXP = models.ManyToManyField(Event, related_name='ACAEXP')
    PROCHP = models.ManyToManyField(Event, related_name='PROCHP')
    PROAFF = models.ManyToManyField(Event, related_name='PROAFF')
    PROEXP = models.ManyToManyField(Event, related_name='PROEXP')

    def recommend(self, ue):
        e_list = ue.event_list.all()
        type = [0, 0, 0]
        price = [0, 0, 0]
        soc = 0
        aca = 0
        pro = 0
        for e in e_list:
            if e.event_type == 'SOC': 
                type[0] += 1
                soc += 1
            elif e.event_type == 'ACA': 
                type[1] += 1 
                aca += 1
            elif e.event_type == 'PRO': 
                type[2] += 1 
                pro += 1
            if e.approximatePrice == '$': price[0] += 1
            elif e.approximatePrice == '$$': price[1] += 1
            elif e.approximatePrice == '$$$': price[2] += 1
        
        if soc > aca:
            if soc > pro:
                to_recommend = Event.objects.filter(event_type = 'SOC')
        elif pro > aca:
            if pro > soc:
                to_recommend = Event.objects.filter(event_type = 'PRO')
        elif aca > soc:
            if aca > pro:
                to_recommend = Event.objects.filter(event_type = 'ACA')
        else:
            to_recommend = Event.objects.filter(event_type = 'SOC')
        return to_recommend

    def find_events_to_recommend(self, type, price):
        type_list = ['SOC', 'ACA', 'PRO']
        price_type = ['CHP', 'AFF', 'EXP']
        lead_type = ''
        lead_price = ''
        y = 0
        x = 0
        z = 0
        while y < 3:
            if type[y] > x:
                x = type[y]
                lead_type = type_list[y]
            if price[y] > z:
                z = price[y]
                lead_price = price_type[y]
            y += 1
        if lead_type == 'SOC':
            if lead_price == "CHP":
                to_recommend = self.SOCCHP.all()
            elif lead_price == 'AFF':
                to_recommend = self.SOCAFF.all()
            else:
                to_recommend = self.SOCEXP.all()
        elif lead_type == 'ACA':
            if lead_price == "CHP":
                to_recommend = self.ACACHP.all()
            elif lead_price == 'AFF':
                to_recommend = self.ACAAFF.all()
            else:
                to_recommend = self.ACAEXP.all()
        elif lead_type == 'PRO':
            if lead_price == "CHP":
                to_recommend = self.PROCHP.all()
            elif lead_price == 'AFF':
                to_recommend = self.PROAFF.all()
            else:
                to_recommend = self.PROEXP.all()
        return to_recommend

    def add_event(self, e):
        if e.event_type == 'SOC':
            if e.approximatePrice == "$":
                self.SOCCHP.add(e)
            elif e.approximatePrice == '$$':
                self.SOCAFF.add(e)
            else:
                self.SOCEXP.add(e)
        elif e.event_type == 'ACA':
            if e.approximatePrice == "$":
                self.ACACHP.add(e)
            elif e.approximatePrice == '$$':
                self.ACAAFF.add(e)
            else:
                self.ACAEXP.add(e)
        else:
            if e.approximatePrice == "$":
                self.PROCHP.add(e)
            elif e.approximatePrice == '$$':
                self.PROAFF.add(e)
            else:
                self.PROEXP.add(e)

    def set_engine(self):
        self.save()


def singleton_engine():
    try:
        re = RecommendationEngine.objects.get(id=1)
    except Exception:
        re = RecommendationEngine()
        re.set_engine()
    return re
