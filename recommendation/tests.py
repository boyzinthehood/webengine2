from django.test import TestCase
import recommendation.models as models
from recommendation.models import Event
from recommendation.models import UserEvents
from django.contrib.auth.models import User
from datetime import date, time
from recommendation.models import RecommendationEngine
import event_creator

class EventTestCase(TestCase):
    def setUp(self):
        Event.objects.create(
		title = "test_event",
		date = date(1995,12,3),
		startTime = '10:00 AM',
        duration_in_minutes = 60,
        approximatePrice = 100
        )
    def test_get_approx_price(self):
        test_event = Event.objects.get(title = "test_event")
        self.assertEqual(test_event.get_approx_price(), 100)

    def test_get_date(self):
        test_event = Event.objects.get(title = "test_event")
        self.assertEqual(test_event.get_date(), date(1995,12,3))

    def test_get_startTime(self):
        test_event = Event.objects.get(title = "test_event")
        self.assertEqual(test_event.get_startTime(), '10:00 AM')

    def test_get_duration_in_minutes(self):
        test_event = Event.objects.get(title = "test_event")
        self.assertEqual(test_event.get_date_duration_in_minutes(), 60)

    def test_create_event(self):
        host = User('user','pass')
        models.create_event('test_title',date(2016,12,3),time(0,0,0,0),24,host,'test_email@yahoo.com',100)
        test_event = Event.objects.get(title = 'test_title')
        self.assertIsNotNone(test_event)                                                                              
        	
        
class UserTestCase(TestCase):
    def setUp(self):
        pass
    def test_create_user(self):
        models.create_user('create_user_test_user', 'cu_tup','hello@yahoo.com','test','user')
        test_user = User.objects.get(username = 'create_user_test_user')
        self.assertIsNotNone(test_user)
    def test_get_previous_events(self):
        pass
    def test_get_friends(self):
        pass



class RecommendationViewTestCase(TestCase):
    def setUp(self):
        models.create_user('test_user','password','test@yahoo.com','test','user') 
    def test_index(self):
        resp = self.client.get('/index/')
        self.assertEqual(resp.status_code,200)
    def test_login_view(self):
        resp = self.client.login(username='test_user',password='password')
        self.assertEqual(resp,True)
    def test_eventslist(self):
        resp = self.client.get('/eventslist/')
        self.assertEqual(resp.status_code,200)
    def test_about(self):
        resp = self.client.get('/about/')
        self.assertEqual(resp.status_code,200)


class set_userTest(TestCase):
    def tester(self):
        models.create_user('test_user','password','test@yahoo.com','test','user')
        u1 = User.objects.get(username='test_user')
        u2 = UserEvents.objects.get(id=1)
        self.assertEqual(u1, u2.myUser)


class link_event_test(TestCase):
    def setUp(self):
        u = models.create_user('bob', 'pass', 'cure@email.com', 'bob', 'bob2')
        models.create_event('e1',date(1995,4,6),'5:00', 60, u, 'cure@email.com', '$', 'SOC', 'here', 'fun?')

    def test(self):
        e = Event.objects.get(title='e1')
        u = UserEvents.objects.get(myUser=User.objects.get(username='bob'))
        models.attend_event(u, e)
        e_list = u.event_list.all()
        e2 = e_list[0]
        self.assertEqual(e, e2)


class RecommendationTest(TestCase):

    def setUp(self):
        u = models.create_user('bob', 'pass', 'cure@email.com', 'bob', 'bob2')
        models.create_event('e1',date(1995,4,6),'5:00', 60, u, 'cure@email.com', '$', 'SOC', 'here', 'fun?')

    def test_add(self):
        re = RecommendationEngine()
        re.set_engine()
        e1 = Event.objects.get(title='e1')
        re.add_event(e1)
        e2 = re.SOCCHP.all()
        e3 = e2[0]
        self.assertEqual(e1, e3)

    def test_large_add(self):
        re = RecommendationEngine()
        re.set_engine()
        models.create_user("jcur", "461995", "curewitz@yahoo.com", "Justin", "Curewitz")
        models.create_user("jzhang", "321995", "fag@yahoo.com", "James", "Zhang")
        models.create_user("kwang", "111995", "kwang@yahoo.com", "Kristian", "Wang")
        models.create_user("csimp", "111995", "csimp@yahoo.com", "Clint", "Simpson")
        models.create_user("jthom", "111995", "jthom@yahoo.com", "Joey", "Thometz")
        e_list = event_creator.make_events()
        for e in e_list:
            re.add_event(e)
        e_list2 = re.SOCCHP.all()
        for i in range(len(e_list2)):
            self.assertEqual(e_list[i], e_list2[i])
        #ue = UserEvents.objects.get(myUser=User.objects.get(username='jcur'))
        #re.recommend(UserEvents.objects.get(ue))

    def test_recommend(self):
        re = RecommendationEngine()
        re.set_engine()
        jc = models.create_user("jcur", "461995", "curewitz@yahoo.com", "Justin", "Curewitz")
        models.create_user("jzhang", "321995", "fag@yahoo.com", "James", "Zhang")
        models.create_user("kwang", "111995", "kwang@yahoo.com", "Kristian", "Wang")
        models.create_user("csimp", "111995", "csimp@yahoo.com", "Clint", "Simpson")
        models.create_user("jthom", "111995", "jthom@yahoo.com", "Joey", "Thometz")
        e_list = event_creator.make_events()
        for e in e_list:
            re.add_event(e)
        ue = UserEvents.objects.get(myUser=User.objects.get(username='jzhang'))
        e_list = re.recommend(ue)
        e_list2 = re.SOCCHP.all()
        for i in range(len(e_list2)):
            self.assertEqual(e_list[i], e_list2[i])

    def test_diff_events(self):
        re = RecommendationEngine()
        re.set_engine()
        jc = models.create_user("jcur", "461995", "curewitz@yahoo.com", "Justin", "Curewitz")
        models.create_user("jzhang", "321995", "fag@yahoo.com", "James", "Zhang")
        models.create_user("kwang", "111995", "kwang@yahoo.com", "Kristian", "Wang")
        models.create_user("csimp", "111995", "csimp@yahoo.com", "Clint", "Simpson")
        models.create_user("jthom", "111995", "jthom@yahoo.com", "Joey", "Thometz")
        e_list = event_creator.make_events()
        for e in e_list:
            re.add_event(e)
        ue = UserEvents.objects.get(myUser=User.objects.get(username='jcur'))
        e_list = re.recommend(ue)
        e_list2 = re.PROCHP.all()
        for i in range(len(e_list)):
            self.assertEqual(e_list[i], e_list2[i])



# Create your tests here.
