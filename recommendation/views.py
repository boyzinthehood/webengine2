from django.shortcuts import render, redirect, HttpResponse
from django.views.generic.edit import CreateView, DeleteView
from django.views.generic import ListView
from recommendation.models import Event
from login import forms
from recommendation.models import create_user, singleton_engine, attend_event
from recommendation.models import create_event,RecommendationEngine, UserEvents
from django.contrib.auth.models import User
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth import authenticate, login, logout, get_user_model


class EventListView(ListView):
    model = Event
    fields = ['title','date', 'duration_in_minutes','host','email']
    template_name = 'recommendation/event_list.html'


#class EventCreate(CreateView):
#    model = Event
#    fields = ['title','date','startTime', 'duration_in_minutes', 'approximatePrice', 'email']
#    template_name = 'recommendation/edit_event.html'


def index(request):
    re = singleton_engine()
    if request.user.is_anonymous() == False:
        ue = UserEvents.objects.get(myUser=request.user)
        e_list = re.recommend(ue)
        if len(e_list) == 0:
            context_dict = {'events_list' : Event.objects.all()}
        else:
            context_dict = {'events_list' : e_list}
    else: context_dict = {'events_list' : Event.objects.all()}
    return render(request, 'recommendation/landing_page.html',context_dict)


def login_view(request):
    if request.method == 'POST':
        form = forms.LoginForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            print(data)
            print(data["username"])
            print(request.POST.get('username'))
            user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
            print(user)
            if user!= None:
                login(request, user=user)
                return redirect('/')
            else:
                print('no user')
                print('why')
        else: print('invalid')
    else:
        form = forms.LoginForm()

    return render(request, 'recommendation/login_page.html',{'form':form})


def eventslist(request):
   context_dict = {'events_list' : Event.objects.all() }
   return render(request, 'recommendation/event_list.html',context_dict)


def about(request):
   context_dict = {}
   return render(request, 'recommendation/about.html',context_dict)


def register(request):
    if request.method == "POST":
        form = forms.RegisterForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            ue = create_user(data['username'], data['password'], data['email'], data['first_name'], data['last_name'])
            print('new user created: ' + data['username'])
            user = authenticate(username=data['username'], password=data['password'])
            print(user)
            if user!= None:
                login(request, user=user)
                return redirect('/')
            return redirect('/')
    else:
        print('invalid form')
        form = forms.RegisterForm()
    return render(request, 'recommendation/register.html', {'form':form})


def logout_page(request):
    context_dict = {}
    logout(request)
    return render(request, 'recommendation/logout_page.html',context_dict)


def add_event(request):
    if request.method == "POST":
        form = forms.AddEventForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            print (request.user)
            create_event(data['title'], data['date'], data['startTime'], data['duration_in_minutes'], request.user, data['email'], data['approximatePrice'], data['event_type'], data['location'], data['description'])
            print(data['title'])
            #print('New event ' + data['title'] + ' created by ' + request.user)
            return redirect('/')
    else:
        print('invalid form')
        form = forms.AddEventForm()
    return render(request, 'recommendation/edit_event.html', {'form':form})


def event_base(request, event):
    if request.method == "GET":
        print('event_base')
        e = Event.objects.get(page=event)
        cu = event.replace(" ", "-")
        return render(request, 'recommendation/event_base.html', {'event':e, 'chatlink': cu})
    elif request.method == "POST":
        print('hope')
        e = Event.objects.get(page=event)
        print(e.page)
        cu = event.replace(" ", "-")
        u = User.objects.get(username=request.user.username)
        ue = UserEvents.objects.get(myUser=u)
        attend_event(ue, e)
        print(ue.event_list.all())
        return render(request, 'recommendation/event_base.html', {'event':e, 'chatlink': cu})
    else:
        print('wrong redirect')

